#include <cassert>
#include <cstdio>
#include <tuple>
#include <set>

#define MAX 200000

int NANI[MAX];
int n,m;
std::tuple<int,int,int> RICHIESTE[MAX];


void checkContinuityBetween(int h1, int h2){

    // calculate prefix sum
    int sum = 0;
    for(int i = h2; i >= h1; i--)
        sum += i;

    int newSum = 0;
    bool startToCount = false;

    for(int i = 0; i < n; i++){
        if(NANI[i] >= h1 && NANI[i] <= h2)
            startToCount = true;

        if(startToCount == true){
            for(int j = 0; j < h2-h1+1; j++){
                newSum += NANI[i++];
            }

            if(newSum == sum)
                printf("YES\n");
            else
                printf("NO\n");
            return;
        }
    }
}


void swappaNani(int p1, int p2){
    int temp = NANI[p1];
    NANI[p1] = NANI[p2];
    NANI[p2] = temp;
}

int main(void){

    /*
#ifdef EVAL
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
#endif
*/

    scanf("%d", &n); // leggi numero nani
    scanf("%d", &m); // leggi numero richieste

    assert(n > 1 && n <= MAX);
    assert(m > 1 && m <= MAX);

    // leggi ordine nani
    for(int  i = 0; i < n; i++)
        scanf("%d", &NANI[i]);

    // leggi richieste
    for(int  i = 0; i < m; i++){
        scanf("%d", &std::get<0>(RICHIESTE[i]));
        scanf("%d", &std::get<1>(RICHIESTE[i]));
        scanf("%d", &std::get<2>(RICHIESTE[i]));
    }

    for (auto t : RICHIESTE){
        if(std::get<0>(t) == 1)
            swappaNani(std::get<1>(t)-1, std::get<2>(t)-1);
        else if(std::get<0>(t) == 2)
            checkContinuityBetween(std::get<1>(t), std::get<2>(t));
    }

    return 0;
}
