#include <cstdio>
#include <cassert>
#include <vector>

#define NMAX 50

int N;
int seq[NMAX];

void borse(int da_assegnare, int massimo, int attuale) {

    // se non ho più nulla da assegnare stampo la sequenza degli assegnamenti
    if (da_assegnare == 0) {
        for (size_t i = 0; i < attuale; i++) {
            printf("%d ", seq[i]);
        }
        printf("\n");
    }

    // altrimenti
    else {
        for (int val = 1; val <= std::min(da_assegnare, massimo); val++) {
            seq[attuale] = val;
            borse(da_assegnare - val, val , attuale+1);
        }
    }
}

int main() {

    #ifdef EVAL
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    #endif

    scanf("%d", &N);

    assert(1 <= N && N <= NMAX);

    borse(N,N,0);

    return 0;
}
