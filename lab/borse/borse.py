from __future__ import print_function

result = [0]*50

def borse(da_assegnare, massimo, attuale):

    if da_assegnare == 0:
        for i in range(0, attuale, +1):
            print(str(result[i]), file = fout, end=' ')
        print(file = fout, end = "\n")

    else:
        for val in range(1, min(da_assegnare, massimo)+1):
            result[attuale] = val
            borse(da_assegnare - val, val , attuale+1)

if __name__ == '__main__':
     fin=open('input.txt','r')
     fout=open('output.txt','w')
     N=int(fin.readline())

     borse(N,N,0)

     fout.close()
