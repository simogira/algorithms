/*
ASSUNZIONI:
N ≤ 1000
Distanza tra le due configurazioni ≤ 2000000
*/

#include <cstdio>
#include <fstream>
#include <cassert>
#include <vector>

std::vector<std::vector<int>> V(6);
int N;

void stampa() {
    for (std::vector<int> v: V){
        for (int i: v)
            printf("%d ", i);
        printf("\n");
    }
}

/*
void risolvi( ) {


    // se il disco in cima al piolo k è minore del disco in cima al piolo j (k!=j)
    // allora è possibile mettere il disco in cima al piolo k, sopra al piolo j

    if (k != j && V[k].back() < V[j].back()) {
        // code
    }

    // risolvo un passo di ogni piolo
    for (size_t piolo = 0; piolo < 3; piolo++) {
        // code
    }
}
*/


int main() {

#ifdef EVAL
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
#endif

    scanf("%d", &N);
    assert(N <= 1000);

    int temp = 0;
    for (int i = 0; i < 6; i++) {
        do {
            scanf("%d", &temp);
            V[i].push_back(temp);
        } while(temp != 0);
    }

    stampa();
    //risolvi();

    return 0;
}
