#include <cstdio>
#include <cassert>

int N;

int main() {

    #ifdef EVAL
        freopen("input.txt", "r", stdin);
        freopen("output.txt", "w", stdout);
    #endif

    scanf("%d", &N);
    printf("%d", N);


    return 0;
}
