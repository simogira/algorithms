//
//  rightwards.cpp
//  
//
//  Created by Simone Girardi on 01/03/2018.
//

#include "rightwards.hpp"
#include <iostream>
#include <fstream>
using namespace std;

void printMatrix(int M[], int rows, int cols){
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++)
            cout << M[i * cols + j] << ' ';
        cout << endl;
    }
}

// return max number of gived column
int findMax(int M[], int rows, int cols int c_index){
    int max = M[c_index];
    for (int i = 1; i < rows; i++) {
        if (max < M[i*cols+c_index])
            max = M[i*cols+c_index];
    }
    return max;
}

// check ....


int main(int argc, char* argv[]) {
    
    int rows, cols;
    int sum, max;
    
    ifstream inputFile (argv[1]);
    if (inputFile.is_open())
    {
        // read size of matrix
        inputFile >> rows >> cols;
        cout << rows << ' ';
        cout << cols << endl;
        
        int M[rows * cols];
        
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                inputFile >> M[i * cols + j];
            }
        }
        printMatrix(M,rows,cols);
        
        // start the algorithm..
        if (rows == 1 && cols == 1) {
            cout << M[0];
        }
        // a row vector
        else if (rows == 1){
            for (int i = 0 ; i < cols; i++) {
                sum += M[i];
            }
        }
        // a column vector
        else if (cols == 1){
            sum = findMax(M,rows, cols, 0);
        }
        
        
        
        
        
        
        
        cout << sum << endl;
        inputFile.close();
    }
    
    else cout << "Unable to open file";

    
    
    return 0;
}
