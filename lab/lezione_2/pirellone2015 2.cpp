//
//  pirellone.cpp
//  
//
//  Created by Simone Girardi on 05/03/2018.
//

//#include "pirellone2015.hpp"

#include <iostream>
#include <fstream>
#include <sstream>      // std::stringstream
#include <string>       // std::string

using namespace std;

void printMatrix(int M[], int rows, int cols){
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++)
            cout << M[i * cols + j] << ' ';
        cout << endl;
    }
}

void printArray(int A[], int size){
    for (int i = 0; i < size; i++)
        cout << A[i] << ' ';
    cout << endl;
}

bool hasBadPattern(int M[], int cols, int i, int j){
    int count = 0;
    
    count += M[i*cols+j];
    count += M[i*cols+j+1];
    count += M[(i+1)*cols+j];
    count += M[(i+1)*cols+j+1];
    
    return ((count % 2) == 0) ? false : true;
}

int findMax(int A[], int size){
    int index = 0;
    int max = A[0];
    for (int i = 1; i < size; i++) {
        if (max < A[i]) {
            max = A[i];
            index = i;
        }
    }
    return index;
}

bool solved(int ROW_COUNT[], int rows){
    int val = 0;
    for (int i = 0; i < rows; i++) {
        val += ROW_COUNT[i];
    }
    return (val == 0);
}

void swapCol(int M[], int cols, int rows, int colMaxIdx){
    for (int i = 0; i < rows; i++) {
        M[i*cols+colMaxIdx] = 1 - M[i*cols+colMaxIdx];
    }
}

void swapRow(int M[], int cols, int rowMaxIdx){
    for (int i = 0; i < cols; i++) {
        M[rowMaxIdx*cols+i] = 1 - M[rowMaxIdx*cols+i];
    }
}

void writeToFile(int OUT_R[], int OUT_C[], int rows, int cols){
    ofstream outputFile;
    outputFile.open ("output.txt");
    for (int i = 0; i < rows; i++)
        outputFile << OUT_R[i] << ' ';
    outputFile << "\n";
    for (int i = 0; i < cols; i++)
        outputFile << OUT_C[i] << ' ';
    outputFile.close();
}

int main(int argc, char* argv[]) {
    
    int rows, cols;
    
    for (int k = 0; k < 19; k++) {
        
    //ifstream inputFile (argv[1]);
        std::stringstream ss;
        ss << "/Users/simonegirardi/Library/Containers/com.apple.mail/Data/Library/Mail Downloads/265CA5A1-12FB-4911-9034-68C439BE0ECE/input" << k << ".txt";
        cout << "file: " << k << endl;
        string s;
        ss >> s;
        
    ifstream inputFile(s);
    if (inputFile.is_open())
    {
        // read size of matrix
        inputFile >> rows >> cols;
        //cout << rows << ' ';
        //cout << cols << endl;
        
        //int M[rows * cols];
        int R1[rows];
        int R2[rows];
        int C[cols];
        int *OUT_R = new int[rows]();
        int *OUT_C = new int[cols]();
        int *ROW_COUNT = new int[rows]();
        int *COL_COUNT = new int[cols]();
        int rowMaxIdx = 0;
        int colMaxIdx = 0;
        
        
        
        
        // read block 2x2
        for (int i = 0; i < (rows-1); i++) {
            for (int j = 0; j < cols; j++) {
                inputFile >> R1[j];
                if (R1[i] == 1) {
                    OUT_R[i] = 1;
                }

                if (M[i*cols+j] == 1) {
                    
                }
            }
        }
        
        
        
        
        
        
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                inputFile >> M[i * cols + j];
                if (M[i*cols+j] == 1) {
                    ROW_COUNT[i]++;
                    COL_COUNT[j]++;
                }
            }
        }
        inputFile.close();
        //printMatrix(M,rows,cols);
        
        // check for a bad pattern
        for (int i = 0; i < (rows-1); i++) {
            for (int j = 0; j < (cols-1); j++) {
                if(hasBadPattern(M,cols,i,j)){
                    //cout << "Problem without solution" << endl;
                    writeToFile(OUT_R, OUT_C, rows, cols);
                    return 0;
                }
            }
        }
    
        
        while (!solved(ROW_COUNT, rows)) {
            
            // find index of max number of ones in rows and cols
            rowMaxIdx = findMax(ROW_COUNT, rows);
            colMaxIdx = findMax(COL_COUNT, cols);
            
            if (ROW_COUNT[rowMaxIdx] < COL_COUNT[colMaxIdx]) {
                swapCol(M, cols, rows, colMaxIdx);
                
                // update ROW_COUNT e COL_COUNT
                COL_COUNT[colMaxIdx] = rows - COL_COUNT[colMaxIdx];
                OUT_C[colMaxIdx]++;
                for (int i = 0; i < rows; i++) {
                    if (M[i*cols+colMaxIdx] == 1)
                        ROW_COUNT[i]++;
                    else
                        ROW_COUNT[i]--;
                }
            }
            else {
                swapRow(M, cols, rowMaxIdx);
                ROW_COUNT[rowMaxIdx] = cols - ROW_COUNT[rowMaxIdx];
                OUT_R[rowMaxIdx]++;
                for (int i = 0; i < cols; i++) {
                    if (M[rowMaxIdx*cols+i] == 1)
                        COL_COUNT[i]++;
                    else
                        COL_COUNT[i]--;
                }
            }
        }
        
        /*
        cout << "output matrix:" << endl;
        printMatrix(M,rows,cols);
        cout << endl;
         

        cout << "output solution:" << endl;
        printArray(OUT_R,rows);
        printArray(OUT_C,cols);
        cout << endl;
         */
        

        writeToFile(OUT_R, OUT_C, rows, cols);

        delete [] ROW_COUNT;
        delete [] COL_COUNT;
        delete [] OUT_R;
        delete [] OUT_C;
    }
    else
        cout << "Unable to open file";
    }
    
    return 0;
}
