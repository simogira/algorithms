//
//  matita.cpp
//  
//
//  Created by Simone Girardi on 25/03/2018.
//

#include <stdio.h>
#include <iostream>
#include <fstream>

using namespace std;

bool G[100000][1000000];

int main(){
    
    int N, M, A, B, from ,to;
    
    // read data from file
    ifstream inFile;
    inFile.open("input.txt");
    if (!inFile) {
        cerr << "Unable to open file input.txt";
        exit(1);   // call system to stop
    }
    inFile >> N;
    inFile >> M;
    inFile >> A;
    inFile >> B;
    
    for (int i = 0; i < M; i++) {
        inFile >> from;
        inFile >> to;
        G[from-1][to-1] = true;             // with offset = 1
        G[to-1][from-1] = true;
    }
    inFile.close();
    
    // print graph
    for (int i= 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            cout << G[i][j] << " ";
        }
        cout << endl;
    }
    
    // buil data structure for nodes and launch CC algorithm to find Eulerian path
    
    
    
    return 0;
}
