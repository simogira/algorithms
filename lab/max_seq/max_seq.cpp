#include <cstdio>
#include <cassert>

int N;
int values[1000000];

int main() {
    #ifdef EVAL
        freopen("input.txt", "r", stdin);
        freopen("output.txt", "w", stdout);
    #endif

    int max;

    scanf("%d", &N);

    for (int i = 0; i < N; i++) {
        scanf("%d", &values[i]);
    }

    max = values[0];
    for (int i = 1; i < N; i++) {
        max = (max < values[i]) ? values[i] : max;
    }

    printf("%d", max);

    return 0;
}
