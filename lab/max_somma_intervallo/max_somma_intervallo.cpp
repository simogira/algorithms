// SOLUZIONE LINEARE
#include <cstdio>
#include <cassert>

int N;
int values[1000000];

int main() {
    #ifdef EVAL
        freopen("input.txt", "r", stdin);
        freopen("output.txt", "w", stdout);
    #endif

    scanf("%d", &N);
    //assert(N >= 2 && N <=1000000);

    // read values from input file
    for (int i = 0; i < N; i++) {
        scanf("%d", &values[i]);
    }

    int max_sum = values[0];

    for (int j = 0, sum = 0; j < N; j++) {
        sum += values[j];
        if (sum > max_sum) {
            max_sum = sum;
        }
        else if(sum < 0){
            sum = 0;
        }
    }

    printf("%d", max_sum);

    return 0;
}
