// SOLUZIONE QUADRATICA

#include <cstdio>
#include <cassert>

int N;
int values[1000000];
int memo[1000000] = {0};
int max_sum;


void sum_interval(int i, int j){

    if (j == N)
        return;

    // memoize the previous sum
    memo[i] += values[j];

    if (max_sum < memo[i])
        max_sum = memo[i];

    sum_interval(i+1, j+1); // check the sum for the next interval
}

int main() {
    #ifdef EVAL
        freopen("input.txt", "r", stdin);
        freopen("output.txt", "w", stdout);
    #endif

    scanf("%d", &N);

    // read values from input file
    for (int i = 0; i < N; i++) {
        scanf("%d", &values[i]);
    }

    max_sum = values[0];

    // for each interval size check the sum
    for (int j = 0; j < N; j++) {
        sum_interval(0, j);
    }

    printf("%d", max_sum);

    return 0;
}
