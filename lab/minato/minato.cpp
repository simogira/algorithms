//
//  minato.cpp
//  
//
//  Created by Simone Girardi on 29/03/2018.
//

#include <stdio.h>
#include <iostream>
#include <fstream>

using namespace std;

char GROUND[1000][1000];

int explore(int i, int j, int M, int N, int L, int number_of_paths){
    
    // if there isn't no possible paths
    if (number_of_paths == 0) {
        return 0;
    }
    
    
    if ((L < 0) || GROUND[i][j] == '+') {
        return 0;
    }
    
    // if goal reached
    if (i == M-1 && j == N-1) {
        return 1;
    }
    else if (i == M || j == N){
        return 0;
    }
    
    return explore(i,j+1, M, N, L-1, number_of_paths) + \
           explore(i+1,j+1, M, N, L-1, number_of_paths) + \
           explore(i+1,j, M, N, L-1, number_of_paths);

    
}

void printGraph(int M, int N){
    for (int i= 0; i < M; i++) {
        for (int j = 0; j < N; j++) {
            cout << GROUND[i][j] << " ";
        }
        cout << endl;
    }
}

int main(){
    
    int N, M, L;
    char temp;
    
    // read data from file
    ifstream inFile;
    
    inFile.open("input3.txt");
    if (!inFile) {
        cerr << "Unable to open file input.txt";
        exit(1);   // call system to stop
    }
    
    inFile >> M;
    inFile >> N;
    inFile >> L;
    
    for (int i = 0; i < M; i++) {
        for (int j = 0; j < N; j++) {
            inFile >> GROUND[i][j];
        }
    }
    inFile.close();
    
    // print graph
    printGraph(M,N);
    
    int number_of_square = (M-1) * (N - 1);
    int number_of_paths = 3 * number_of_square - 1;
    
    cout << "Il numero di cammini possibili e': " << number_of_paths << endl;
    
    int valid_paths = 0;
    
    valid_paths = explore(0,0, M, N, L, number_of_paths);
    
   
    cout << "valid paths: " << valid_paths << endl;
    
    
    
 
    
    
    
    return 0;
}
