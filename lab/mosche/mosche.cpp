/*
Subtasks
• Subtask 0 [1 punto]: i tre esempi del testo.
• Subtask 1 [29 punti]: gli altri 20 esempi alle COCI.
• Subtask 2 [30 punti]: K ≤ 100, R, S ≤ 2 000.
• Subtask 3 [40 punti]: 3 ≤ K ≤ R, S ≤ 2 000.
*/
#include<iostream>
#include<fstream>
#include<cstdio>

using namespace std;

bool PIXELS[2000][2000];
//int MEMO[2000][2000] = {0}; // sum of successive vertical k elements

int R,S,K, K2;
int prev_flies = 0;
int max_flies = 0;
int max_r,max_c;

void compare_max(int a, int i, int j){
    if (a > max_flies) {
        max_flies = a;
        max_r = i;
        max_c = j;
    }
}

/*
void fill_memo() {
    for (size_t j = 1; j < S; j++) {
        for (size_t t = 0; t < K2; t++) {
            MEMO[1][j] += PIXELS[t+1][j];
        }
        for (size_t i = 2; i < R-K2; i++) {
            MEMO[i][j] = MEMO[i-1][j] + PIXELS[i+K2-1][j] - PIXELS[i-1][j];
        }
    }
}
*/

void print_racket (int i, int j){
    if ((i == max_r && j == max_c) ||
    (i == max_r && j == max_c + K-1) ||
    (i == max_r + K-1 && j == max_c) ||
    (i == max_r + K-1 && j == max_c + K-1))
    {
        printf("+");
    }
    else if (j == max_c || j == max_c + K-1) {
        printf("|");
    }
    else if (i == max_r || i == max_r + K-1) {
        printf("-");
    }
    else{
        printf("%c", PIXELS[i][j]? '*' : '.');
    }
}

void  printMonitor() {
    // print monitor

    //printf("max_r = %d\n", max_r );
    //printf("max_c = %d\n", max_c );
    for (int i = 0; i < R; i++) {
        for (int j = 0; j < S; j++){
            if (j >= max_c && j < max_c + K && i >= max_r && i < max_r + K) {
                print_racket(i,j);
            }
            else
                printf("%c", PIXELS[i][j]? '*' : '.');
        }
        printf("\n");
    }
}


int memo(int r, int c){
    int sum = 0;
    for (size_t i = 0; i < K2; i++) {
        sum += PIXELS[r+i][c];
    }
    return sum;
}


int main() {

#ifdef EVAL
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
#endif

    char dio;

    //ifstream inFile("input.txt");
    //ofstream outFile("output.txt");

    //cout.rdbuf(outFile.rdbuf()); //redirect std::cout to out.txt!

    scanf("%d", &R);
    scanf("%d", &S);
    scanf("%d", &K);

    K2 = K-2;


    // fill the monitor
    for (int i = 0; i < R; i++) {
        for (int j = 0; j < S; j++){
            scanf("%c",  &dio);
            if (dio != ' ' && dio != '\n') {
                PIXELS[i][j] = dio == '*';
            }
            else
                j--;
        }
    }

    //fill_memo();

/*
    for (size_t i = 0; i < R; i++) {
        for (size_t j = 0; j < S; j++) {
            printf("%d", MEMO[i][j] );
        }
        printf("\n");
    }
*/
    for (size_t i = 0; i <= R-K; i++) {
        prev_flies = 0;
        // first racket
        for (size_t t = 1; t < K-1; t++) {
            prev_flies += memo(i+1,t);
        }

        //printf("prev_flies 1: %d\n", prev_flies);

        compare_max(prev_flies,i ,0);
        // others rackets
        for (size_t j = 1; j <= S-K; j++) {
            prev_flies += memo(i+1,j+K2) - memo(i+1,j);
            compare_max(prev_flies,i ,j);
        }
        //printf("prev_flies 2: %d\n", prev_flies);

    }

    printf("%d\n", max_flies);
    printMonitor();

    return 0;
}
