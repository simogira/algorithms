/*
Subtasks
• Subtask 0 [1 punto]: i tre esempi del testo.
• Subtask 1 [29 punti]: gli altri 20 esempi alle COCI.
• Subtask 2 [30 punti]: K ≤ 100, R, S ≤ 2 000.
• Subtask 3 [40 punti]: 3 ≤ K ≤ R, S ≤ 2 000.
*/
#include<cassert>
#include<iostream>
#include<fstream>
#include<cstdio>

using namespace std;

const int MAX_R = 2000;
const int MAX_C = 2000;

bool PIXELS[MAX_R +5][MAX_C +5];
int MEMO[MAX_R +5][MAX_C +5]; // sum of successive vertical k elements

int R,S,K, K2;
int max_flies = 0;
int max_r,max_c;

void compare_max(int a, int i, int j){
    if (a > max_flies) {
        max_flies = a;
        max_r = i;
        max_c = j;
    }
}

void fill_memo() {
    for (size_t j = 0; j < S; j++) {
        for (size_t t = 0; t < K2; t++) {
            MEMO[0][j] += PIXELS[t][j];
        }
        for (size_t i = 1; i < R-K2; i++) {
            MEMO[i][j] = MEMO[i-1][j] + PIXELS[i+K2-1][j] - PIXELS[i-1][j];
        }
    }
}

void print_racket (int i, int j){
    if (((i == max_r) || (i == max_r + K-1))
    && ((j == max_c) || (j == max_c + K-1)))
    {
        printf("+");
    }
    else if (j == max_c || j == max_c + K-1) {
        printf("|");
    }
    else if (i == max_r || i == max_r + K-1) {
        printf("-");
    }
    else{
        printf("%c", PIXELS[i][j]? '*' : '.');
    }k
}

void  printMonitor() {
    // print monitor

    //printf("max_r = %d\n", max_r );
    //printf("max_c = %d\n", max_c );
    assert(max_r >= 0); assert(max_r+K-1 <= R);
    assert(max_c >= 0); assert(max_c+K-1 <= S);
    for (int i = 0; i < R; i++) {
        for (int j = 0; j < S; j++){
            print_racket(i,j);
        }
        printf("\n");
    }
}

int main() {

#ifdef EVAL
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
#endif


    //ifstream inFile("input.txt");
    //ofstream outFile("output.txt");

    //cout.rdbuf(outFile.rdbuf()); //redirect std::cout to out.txt!

    scanf("%d", &R);
    scanf("%d", &S);
    scanf("%d", &K);

    K2 = K-2;


    // fill the monitor
    for (int i = 0; i < R; i++)
        for (int j = 0; j < S; j++){
            char dio=' ';
            while(dio != '*' && dio != '.')
               scanf("%c",  &dio);
            PIXELS[i][j] = dio == '*';
        }

    fill_memo();

/*
    for (size_t i = 0; i < R; i++) {
        for (size_t j = 0; j < S; j++) {
            printf("%d", MEMO[i][j] );
        }
        printf("\n");
    }
*/
    for (int i = 0; i+K-1 < R; i++) {
        int prev_flies = 0;
        // first racket
        for (size_t t = 0; t < K2; t++) {
            prev_flies += MEMO[i+1][t+1];
        }

        //printf("prev_flies 1: %d\n", prev_flies);

        compare_max(prev_flies,i ,0);
        // others rackets
        for (size_t j = 1; j+K-1 < S; j++) {
            prev_flies += MEMO[i+1][j+K2] - MEMO[i+1][j];
            compare_max(prev_flies,i ,j);
        }
        //printf("prev_flies 2: %d\n", prev_flies);

    }


    printf("%d\n", max_flies);
    printMonitor();

    return 0;
}
