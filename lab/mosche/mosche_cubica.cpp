/*
Subtasks
• Subtask 0 [1 punto]: i tre esempi del testo.
• Subtask 1 [29 punti]: gli altri 20 esempi alle COCI.
• Subtask 2 [30 punti]: K ≤ 100, R, S ≤ 2 000.
• Subtask 3 [40 punti]: 3 ≤ K ≤ R, S ≤ 2 000.
*/
#include<iostream>
#include<fstream>

using namespace std;

char PIXELS[2000][2000];
int R,S,K;
int max_flies = 0;
int max_r,max_c;

void print_racket (int i, int j){
    if ((i == max_r && j == max_c) ||
    (i == max_r && j == max_c + K-1) ||
    (i == max_r + K-1 && j == max_c) ||
    (i == max_r + K-1 && j == max_c + K-1))
    {
        cout << '+';
    }
    else if (j == max_c || j == max_c + K-1) {
        cout << '|';
    }
    else if (i == max_r || i == max_r + K-1) {
        cout <<  '-';
    }
    else{
        cout << PIXELS[i][j];
    }
}

void  printMonitor() {
    // print monitor
    for (int i = 0; i < R; i++) {
        for (int j = 0; j < S; j++){
            if (j >= max_c && j < max_c + K && i >= max_r && i < max_r + K) {
                print_racket(i,j);
            }
            else
                cout << PIXELS[i][j];
        }
        cout << endl;
    }
}

void eval(int r, int c){

    if (r+K > R || c+K > S){
        //std::cout << "return.." << '\n';
        return;
    }

    int flies = 0;
    // cAlculate max_flies inner k x k matrix
    for (int i = r+1; i < r+K-1; i++) {
        for (int j = c+1; j < c+K-1; j++) {
            if (PIXELS[i][j] == '*') // if there is a fly
            flies++;
        }
    }

    if (max_flies < flies) {
        max_flies = flies;
        max_r = r;
        max_c = c;
    }

    //eval(r+1, c);
    eval(r,c+1);

}

int main() {
    //int R,S,K;

    ifstream inFile("input.txt");
    ofstream outFile("output.txt");

    cout.rdbuf(outFile.rdbuf()); //redirect std::cout to out.txt!

    //ofstream outFile("output.txt");

    inFile >> R; inFile >> S; inFile >> K;
    //std::cout << "R:" << R << '\n'<< "S:" << S << '\n'<< "K:" << K << '\n';

    // fill the monitor
    for (int i = 0; i < R; i++) {
        for (int j = 0; j < S; j++)
        inFile >> PIXELS[i][j];
    }

    for (size_t i = 0; i < R; i++) {
        eval(i,0);
    }

    cout << max_flies << '\n';
    printMonitor();



    return 0;
}
