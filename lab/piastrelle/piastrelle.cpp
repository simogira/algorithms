//
//  pirellone.cpp
//
//
//  Created by Simone Girardi on 05/03/2018.
//

//#include "pirellone2015.hpp"

#include <iostream>
#include <assert.h>
#include <string.h>
#include <fstream>

#define MAX_N 25

using namespace std;

ifstream inFile;
ofstream outFile;

/*
int next(int n){
    if (n <= 1) return 1;
    
    return next(n-1) + next(n-2);
}
*/

void printList(std::string N[], int size){
    for (int i = 0; i < size; i++) {
        outFile << N[i];
    }
    outFile << "\n";
}



void list(std::string N[], int n, int pos){
    
    assert(n >= 0);
    
    // ad ogni passo abbiamo 0-1-2 possibilità
    
    if (n < 1){
        // non posso più mettere giu nessuna piastrella, stampo e ritorno
        printList(N, pos);
        return;
    }
    
    // metto giu una piastrella da 1 e vado avanti
    N[pos] = "[O]";
    list(N, n-1, pos+1);
    
    // e se c'e' posto posso mettere giu' una piastrella da 2
    if (n >= 2) {
        N[pos] = "[OOOO]";
        list(N, n-2, pos+1);
    }

}



int main(int argc, char* argv[]) {
    
    std::string N[MAX_N];
    int n;
    
    inFile.open("input.txt");
    outFile.open("output.txt");
    
    if (!inFile) {
        cerr << "Unable to open file datafile.txt";
        exit(1);   // call system to stop
    }
    inFile >> n;
    inFile.close();
    
    /*
    printf("Inserisci n: ");
    scanf("%d", &n);
     */
    
    assert(n >= 1 && n <= MAX_N);
    
    //cout << next(n) << endl;
    list(N, n, 0);

    outFile.close();

    return 0;
}
