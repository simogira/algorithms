f1 = '[O]'
f2 = '[OOOO]'

def piastrelle(F, lista, m):

    if m < 1:
        F.append(''.join(map(str, lista)))
        return

    piastrelle(F, lista + [f1], m-1)

    if m >= 2:
        piastrelle(F, lista + [f2], m-2)

    return F

if __name__ == '__main__':
     fin=open('input.txt','r')
     fout=open('output.txt','w')
     N=int(fin.readline())
     F=[]
     lista=[]

F=piastrelle(F, lista, N)


for i in range(len(F)):
    fout.write(F[i][:]+'\n') # bisogna convertire la stringa

fout.close()
