/*
ASSUNZIONI:
    1 ≤ N ≤ 30
*/
#include <cstdio>
#include <cassert>

int N;
long long int modi = 0;

void pillole(int intere, int ridotte){

    // se la bottiglia è vuota ho finito
    if (intere + ridotte == 0){
        //printf("\n");
        modi++;
        return;
    }

    // se ho pescato tutte le pillole intere mi è rimasta almeno una pillola ridotta
    if (intere == 0) {
        //printf("M");
        pillole(intere, ridotte-1);
    }

    // se ci sono pilleole intere pesco una pillola intera e ne creo una ridotta
    if (intere > 0) {
        //printf("I");
        pillole(intere-1, ridotte+1);

        if (ridotte > 0) {
            //printf("M");
            pillole(intere, ridotte-1);
        }
    }

}


int main(int argc, char const *argv[]) {

    #ifdef EVAL
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    #endif

    scanf("%d", &N);

    assert(N > 0 && N < 31);

    pillole(N,0);
    //printf("%d\n", pillole(N,0));
    printf("%lld\n", modi);


    return 0;
}


/*
if (ridotte + intere == 0){    // tutte le volte che svuoto la bottiglietta ho trovato un modo
    printf("\n");
    return 1;
}

if (intere == 0) {
    return 0;
}

// ogni volta che pesco una pillola intera ne creo una ridotta
printf("I");
pillole(intere-1, ridotte+1);

printf("M");
return 1 + pillole(intere, ridotte-1);
*/
