# N righe
# M colonne

fin= open('input.txt', 'r')
fout=open('output.txt' , 'w')

N, M = [int(x) for x in next(fin).split()]
m = [[x for x in line.split()] for line in fin]

listarighe = []
listacol = []

# ogni volta che nella prima colonna trovo un 1 modifico tutta la riga
for r in range(N):
     if m[r][0] == '1':
         listarighe.append('1')
         for c in range(M):
             m[r][c] =  ('1' if m[r][c] == '0' else '0')
     else:
         listarighe.append('0')

# ogni volta che nella prima riga trovo un 1 modifico tutta la colonna
for c in range(M):
     if m[0][c] == '1':
         listacol.append('1')
         for r in range(N):
             m[r][c] = ('1' if m[r][c] == '0' else '0')
     else:
        listacol.append('0')

risolvibile = True
for values in m:
    if '1' in values:
        risolvibile = False
        fout.write('0 ' * N + '\n')
        fout.write('0 ' * M)

if risolvibile:
    fout.write(' '.join(map(str, listarighe)) + '\n')
    fout.write(' '.join(map(str, listacol)))

fout.close()
