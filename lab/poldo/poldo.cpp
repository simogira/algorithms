/*
Assunzioni
• 1 ≤ N ≤ 10 000
• 0 ≤ p < 10 000
*/
#include <cstdio>
#include <cassert>
#include <vector>
#include <algorithm>

#define MAX 10000

int P[MAX];
int PRESI[MAX] = {0}; // memoizzazione
int N;
int maxNumeroPaniniMangiabili = 0;

void eval(int i){

  if(i == N)
    return;

  eval(i+1);

  int max = 0;
  for (int j = N-1; j > i; j--){
    if (P[i] > P[j] && PRESI[j] > max)
      max = PRESI[j];
  }
  PRESI[i] = max + 1;
  maxNumeroPaniniMangiabili = std::max(maxNumeroPaniniMangiabili, PRESI[i]);
}

int main(int argc, char const *argv[]) {

#ifdef EVAL
  freopen("input.txt", "r", stdin);
  freopen("output.txt", "w", stdout);
#endif

  scanf("%d", &N);
  assert(N > 0 && N <= MAX);

  for (int i = 0; i < N; i++)
    scanf("%d", &P[i]);

  eval(0);

  printf("%d", maxNumeroPaniniMangiabili);

  return 0;
}
