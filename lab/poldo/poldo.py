import sys
PRESI = [0]*10000 # memoizzazione
maxNumeroPaniniMangiabili = 0

def eval(i):
    global maxNumeroPaniniMangiabili # to change the global value need the global keyword
    global PRESI

    if i == N:
      return

    eval(i+1)

    maximum = 0
    for j in range(N-1, i, -1):
        if P[i] > P[j] and PRESI[j] > maximum:
          maximum = PRESI[j]

    PRESI[i] = maximum + 1
    maxNumeroPaniniMangiabili = max(maxNumeroPaniniMangiabili, PRESI[i])

if __name__ == '__main__':
     sys.setrecursionlimit(100000)
     fin=open('input.txt','r')
     fout=open('output.txt','w')
     N=int(fin.readline())
     P = []

     for i in range(N):
        P.append(int(fin.readline()))

     eval(0)

     fout.write(str(maxNumeroPaniniMangiabili))

fout.close()
