#include <cstdio>
#include <cassert>
#include <iostream>
#include <fstream>

using namespace std;
#define MAX 10000

int soluzioni[MAX];
int panini[MAX];

int main(int argc, char *argv[])
{
  fstream in,out;
  int numeroPanini,max;
  in.open("input.txt",ios::in);
  out.open("output.txt",ios::out);
  in >> numeroPanini;

  assert(numeroPanini > 0 && numeroPanini <= MAX);

  for (int i=0;i<numeroPanini;i++)
    in >> panini[i];

  for (int i=numeroPanini-1;i>=0;i--)
  {
    max=0;
    for (int j=numeroPanini-1;j>i;j--)
    {
      if (panini[i] > panini[j] && soluzioni[j]>max)
        max=soluzioni[j];
    }
    soluzioni[i] = max + 1;

  }
  max=soluzioni[0];
  for (int i=1; i< numeroPanini;i++)
    if (soluzioni[i] > max)
      max = soluzioni[i];
  out << max;
}
