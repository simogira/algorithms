/*
Assunzioni
• 2 ≤ N ≤ 400000
• 1 ≤ P ≤ N
*/

#include <cstdio>
#include <cassert>
#include <utility>      // std::pair, std::make_pair
#include <vector>

#define MAX 400000

int COSTISNODI[MAX];
std::pair<int, int> PISTE[MAX];
std::vector<int> TREE[MAX];

void solve(/* arguments */) {
  /* code */
}

int main(int argc, char const *argv[]) {

  #ifdef EVAL
  freopen("input.txt", "r", stdin);
  freopen("output.txt", "w", stdout);
  #endif

  int numeroSnodi;
  scanf("%d", &numeroSnodi);
  assert(numeroSnodi >= 2 && numeroSnodi <= 400000);

  // leggi gli snodi e i relativi costi
  for (int i = 0; i < numeroSnodi; i++) {
    scanf("%d", &COSTISNODI[i]);
  }

  // leggi le piste che collegano gli snodi e costruisci un albero
  for (int i = 0, val = 0, val2 = 0; i < numeroSnodi-1; i++) {
    scanf("%d", &val);
    scanf("%d", &val2);
    //scanf("%d\n", &PISTE[i].first);
    //scanf("%d\n", &PISTE[i].second);
    val--;
    val2--;
    TREE[val].push_back(val2);
    TREE[val2].push_back(val);

    // decrease values once for semplicity
    //PISTE[i].first--;
    //PISTE[i].second--;
  }

  solve();

  // before print out increase the PISTE values for consistency
  for (int i = 0; i < numeroSnodi-1; i++) {
    /*
    PISTE[i].first++;
    PISTE[i].second++;
    printf("%d ", PISTE[i].first);
    printf("%d\n", PISTE[i].second);
    */
    printf("TREE[%d]: ", i);
    for (int figlio: TREE[i])
        printf("%d ", figlio);
    printf("\n");
  }


  return 0;
}
