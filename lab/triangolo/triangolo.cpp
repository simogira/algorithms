/*
Assunzioni
• 1 < N ≤ 100
• 0 ≤ i < 100
*/
#include <cstdio>
#include <cassert>
#include <vector>

std::vector<int> TRIANGOLO;

// valuta la somma massima di un triangolo di n livelli
void eval(int n) {

    if (n <= 0)
        return;

    for (size_t i = (n*(n-1))/2; i < (n*(n+1))/2; i++) {
        TRIANGOLO[i] += std::max(TRIANGOLO[i+n], TRIANGOLO[i+n+1]);
    }

    eval(n-1);     // valuta figlio sinistro
}

int main() {

#ifdef EVAL
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
#endif

    int N;
    scanf("%d", &N);
    assert(N > 0 && N <= 100);

    int dim = N*(N+1)/2;

    // leggi righe del triangolo
    int val = 0;
    for (size_t i = 0; i < dim; i++) {
        scanf("%d", &val);
        TRIANGOLO.push_back(val);
    }

    eval(N-1);

    printf("%d\n", TRIANGOLO[0]);

    return 0;
}
