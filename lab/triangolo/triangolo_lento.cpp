/*
Assunzioni
• 1 < N ≤ 100
• 0 ≤ i < 100
*/
#include <cstdio>
#include <cassert>
#include <vector>

std::vector<int> TRIANGOLO;
int max = 0;
int N;

// valuta la somma massima di un triangolo di n livelli
void eval(int livello, int offset, int sum, int index) {

    if (livello == N)
        return;

    int i = livello+index+offset;
    sum += TRIANGOLO[i];

    if (max < sum)
        max = sum;

    eval(livello+1, 0, sum, i);     // valuta figlio sinistro
    eval(livello+1, 1, sum, i);     // valuta figlio destro
}

int main() {

#ifdef EVAL
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
#endif

    scanf("%d", &N);
    assert(N > 0 && N <= 100);

    int dim = N*(N+1)/2;

    // leggi righe del triangolo
    int val = 0;
    for (size_t i = 0; i < dim; i++) {
        scanf("%d", &val);
        TRIANGOLO.push_back(val);
    }

    eval(0, 0, 0, 0);

    printf("%d\n", max);

    return 0;
}
